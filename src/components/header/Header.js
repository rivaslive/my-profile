/* eslint-disable jsx-a11y/anchor-is-valid */
import "./Header.css";
import Headroom from "react-headroom";
import React, { useContext } from "react";
import ToggleSwitch from "../ToggleSwitch/ToggleSwitch";
import StyleContext from "../../contexts/StyleContext";
import { greeting, workExperiences } from "../../portfolio";

function Header() {
  const { isDark } = useContext(StyleContext);
  const exp = workExperiences.viewExperiences;
  return (
    <Headroom>
      <header className={isDark ? "dark-menu header" : "header"}>
        <a href="#" className="logo">
          <span className={!isDark ? "logo-name" : "logo-name grey-color"}>{greeting.username}</span>
        </a>
        <input className="menu-btn" type="checkbox" id="menu-btn" />
        <label
          className="menu-icon"
          htmlFor="menu-btn"
          style={{ color: "white" }}
        >
          <span className={isDark ? "navicon navicon-dark" : "navicon"}/>
        </label>
        <ul className={isDark ? "dark-menu menu" : "menu"}>
          <li>
            <a href={"#skills"}>Skills</a>
          </li>
          {exp === true && (
            <li>
              <a href={"#experience"}>Work Experiences</a>
            </li>
          )}
          <li>
            <a href={"#project"}>Project</a>
          </li>
          <li>
            <a href={"#contact"}>Contact Me</a>
          </li>
          <li>
            <a href={"#"}>
              <span><i className='far fa-moon'/><span style={{padding: '0 5px'}}>Night mode</span></span>
              <ToggleSwitch />
            </a>
          </li>
        </ul>
      </header>
    </Headroom>
  );
}
export default Header;
