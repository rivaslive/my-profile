import React from "react";
import "./Button.css";

export default function Button({text, className, href, newTab}) {
	return (
		<div className={className}>
			{
				!!href ?
					<a className="main-button" href={href} target={newTab && "_blank"}>
						{text}
					</a>
					:
					<div className="main-button" style={{marginLeft: 'auto', marginRight: 'auto'}}>
						{text}
					</div>
			}
		</div>
	);
}
