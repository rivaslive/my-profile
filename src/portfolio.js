﻿/* Change this file to get your personal Portfolio */

// Your Summary And Greeting Section

import emoji from "react-easy-emoji";

const greeting = {
	/* Your Summary And Greeting Section */
	username: "Kevin R.",
	title: "Software",
	subTitle: emoji("ING. KEVIN RIVAS"),
	resumeLink: "https://drive.google.com/file/d/1ofFdKF_mqscH8WvXkSObnVvC9kK7Ldlu/view?usp=sharing"
};

// Your Social Media Link

const socialMediaLinks = {
	linkedin: "https://www.linkedin.com/in/kevin-rivas-frontend-developer/",
	gmail: "rl0185032015@unab.edu.sv",
	gitlab: "https://gitlab.com/rivaslive",
	facebook: "https://www.facebook.com/krlopez3",
	// Instagram and Twitter are also supported in the links!
};

// Your Skills Section

const skillsSection = {
	title: "What i do",
	subTitle: "Full stack developer who want to explore every tech stack",
	skills: [
		emoji("⚡ Develop highly interactive Front end / User Interfaces for your web and mobile applications"),
		emoji("⚡ Server Side Rendering(SSR) with Next.js"),
		emoji("⚡ Integration of third party services such as Firebase/ AWS / Digital Ocean")
	],
	
	/* Make Sure You include correct Font Awesome Classname to view your icon
	https://fontawesome.com/icons?d=gallery */
	
	softwareSkills: [
		{
			skillName: "JavaScript",
			fontAwesomeClassname: "fab fa-js"
		},
		{
			skillName: "python",
			fontAwesomeClassname: "fab fa-python"
		},
		{
			skillName: "react.js",
			fontAwesomeClassname: "fab fa-react"
		},
		{
			skillName: "redux.js",
			fontAwesomeClassname: "icon-redux-seeklogocom"
		},
		{
			skillName: "html-5",
			fontAwesomeClassname: "fab fa-html5"
		},
		{
			skillName: "css3",
			fontAwesomeClassname: "fab fa-css3-alt"
		},
		{
			skillName: "sass",
			fontAwesomeClassname: "fab fa-sass"
		},
		{
			skillName: "node.js",
			fontAwesomeClassname: "fab fa-node"
		},
		{
			skillName: "firebase",
			fontAwesomeClassname: "fas fa-fire"
		},
		{
			skillName: "mongoDB",
			fontAwesomeClassname: "icon-mongodb"
		},
		{
			skillName: "aws",
			fontAwesomeClassname: "fab fa-aws"
		},
		{
			skillName: "npm",
			fontAwesomeClassname: "fab fa-npm"
		},
		{
			skillName: "git",
			fontAwesomeClassname: "fab fa-git"
		},
	]
};

// Your education background

const educationInfo = {
	viewEducation: true, // Set it to true to see education section
	schools: [
		{
			schoolName: "Universidad Dr. Andres Bello",
			logo: require("./assets/images/unab.png"),
			subHeader: "System engineering in Computer Science",
			duration: "January 2015 - December 2020",
			desc: "2° Placed in Technology fair ASPRO-CONINFO 2018",
			descBullets: [
				"Python, WebSocket, HTML+CSS",
				"Raspberry pi, Arduino, Domotics"
			]
		},
		{
			schoolName: "Instituto Nacional Dr. Francisco Martinez Suarez",
			logo: require("./assets/images/insfram.jpeg"),
			subHeader: "High School - Technical accounting option",
			duration: "January 2012 - November 2014",
			
		}
	]
}

// Your top 3 proficient stacks/tech experience

const techStack = {
	viewSkillBars: true, //Set it to true to show Proficiency Section
	experience: [
		{
			Stack: "Frontend/Design",  //Insert stack or technology you have experience in
			progressPercentage: "90%"  //Insert relative proficiency in percentage
		},
		{
			Stack: "Backend",
			progressPercentage: "75%"
		},
		{
			Stack: "Programming",
			progressPercentage: "85%"
		},
		{
			Stack: "English",
			progressPercentage: "20%"
		}
	]
};


// Your top 3 work experiences

const workExperiences = {
	viewExperiences: true, //Set it to true to show workExperiences Section
	experience: [
		{
			role: "Frontend Developer",
			company: "Tyvo inc",
			companylogo: require("./assets/images/tyvo-logo.png"),
			date: "Ago 2020 – Dec 2020",
			desc: "Creation Ecommerce with Nextjs, TypeScript y React Context",
			descBullets: [
				"Next.js",
				"TypeScript",
				"React Context",
				"Styled Component",
				"Intl lang",
			]
		},
		{
			role: "Back-End Developer",
			company: "Norttech",
			companylogo: require("./assets/images/aeegle-logo.jpeg"),
			date: "May 2019 – Mar 2020",
			desc: "Microservices for search jobs and career-api",
			descBullets: [
				'Node.js',
				'Express',
				'MongoDB',
				'Eslint',
				'Jira',
				'Confluence',
				'Swagger.io'
			]
		},
		{
			role: "Frontend Developer",
			company: "Ministerio de Cultura de El Salvador",
			companylogo: require("./assets/images/logo.jpeg"),
			date: "Nov 2019",
			desc: "Development of my engineering thesis, An administrator panel of Places, Events, Graphics, Roles, Reviews, etc.",
			descBullets: [
				'React.js',
				'Antd',
				'TypeScript',
				'Firebase'
			]
		},
	]
};

/* Your Open Source Section to View Your Github Pinned Projects
To know how to get github key look at readme.md */

const openSource = {
	githubConvertedToken: process.env.REACT_APP_GITHUB_TOKEN,
	githubUserName: "saadpasta", // Change to your github username to view your profile in Contact Section.
	showGithubProfile: "true" // Set true or false to show Contact profile using Github, defaults to false
};


// Some Big Projects You have worked with your company
const bigProjects = {
	title: "Projects",
	subtitle: "Projects I have worked on",
	projects: [
		{
			title: 'Hola Mundo | TimerCycle',
			description: 'Construction of a list of schedules where every certain amount of time you can choose 8 motorcyclists, these can be used in the valid intervals of the list, you can also release the resources as well as use them again',
			url: "https://gitlab.com/rivaslive/timer-cycle"
		},
		{
			title: 'Tyvo App',
			description: 'Ecommerce ready to roll out, waiting for new drivers. built in NextJs and full Context API.',
			url: "https://tyvoapp.com"
		},
		{
			title: 'Zumpul - Aeegle',
			description: 'Zumpul is a Web Application that help companies to manage all email Signatures for G Suite from one place Now with Zumpul G Suite Administrator or the Marketing department will be able to take the control, manage and standardize email signatures from one place for the entire domain, finally having full control about what employees write in their email signatures.',
			url: "https://app.dev.zumpul.com/login"
		},
		{
			title: 'Journal App',
			description: 'It is an example application, built in React with Redux, using a design line based on ClickUp together with Ant Design, backend in Firebase Function',
			url: "https://journal-app-mu.vercel.app/"
		},
		{
			title: 'Invoice - Hard Rock',
			description: 'It is an ecommerce in development that will be available in El Salvador and the United States, it offers services such as sale of products and transportation services',
			url: "https://hard-rock-granite.web.app/"
		},
		{
			title: 'oncichan',
			description: 'An application that uses a comic character public api, you just have to enter a random username and password to access the site',
			url: "https://oncichan-react.vercel.app/"
		},
		{
			title: 'Inmuebles FE',
			description: 'A real estate landing page',
			url: "https://inmuebles-fe.vercel.app/"
		},
		{
			title: 'antd-notifications-messages',
			description: 'This demo of library antd-notifications-messages',
			url: "https://antd-notifications-messages.vercel.app/"
		}
	]
};

const onWayProjects = {
	title: "On my way!",
	subtitle: "Technologies that I am learning to offer quality to your products",
	projects: [
		{
			title: 'React Native',
			description: 'React Native is an open-source mobile application framework created by Facebook, Inc. It is used to develop applications for Android, Android TV, iOS, macOS, tvOS, Web, Windows and UWP by enabling developers to use React\'s framework along with native platform capabilities.',
			url: "https://reactnative.dev/"
		},
		{
			title: 'Redux Saga',
			description: 'redux-saga is a library that aims to make application side effects (i.e. asynchronous things like data fetching and impure things like accessing the browser cache) easier to manage, more efficient to execute, easy to test, and better at handling failures.',
			url: "https://redux-saga.js.org/"
		},
		{
			title: 'Golang',
			description: 'Go is an open source programming language that makes it easy to build simple, reliable, and efficient software.',
			url: "https://golang.org"
		}
	]
};

// Your Achievement Section Include Your Certification Talks and More

const achievementSection = {
	
	title: emoji("Achievements And Certifications 🏆 "),
	subtitle: "Achievements, Certifications, Award Letters and Some Cool Stuff that I have done !",
	
	achievementsCards: [
		{
			title: "Google Code-In Finalist",
			subtitle: "First Pakistani to be selected as Google Code-in Finalist from 4000 students from 77 different countries.",
			image: require("./assets/images/codeInLogo.webp"),
			footerLink: [
				{
					name: "Certification",
					url: "https://drive.google.com/file/d/0B7kazrtMwm5dYkVvNjdNWjNybWJrbndFSHpNY2NFV1p4YmU0/view?usp=sharing"
				},
				{
					name: "Award Letter",
					url: "https://drive.google.com/file/d/0B7kazrtMwm5dekxBTW5hQkg2WXUyR3QzQmR0VERiLXlGRVdF/view?usp=sharing"
				},
				{name: "Google Code-in Blog", url: "https://opensource.googleblog.com/2019/01/google-code-in-2018-winners.html"}
			]
		},
		{
			title: "Google Assistant Action",
			subtitle: "Developed a Google Assistant Action JavaScript Guru that is available on 2 Billion devices world wide.",
			image: require("./assets/images/googleAssistantLogo.webp"),
			footerLink: [{
				name: "View Google Assistant Action",
				url: "https://assistant.google.com/services/a/uid/000000100ee688ee?hl=en"
			}]
		},
		
		{
			title: "PWA Web App Developer",
			subtitle: "Completed Certifcation from SMIT for PWA Web App Development",
			image: require("./assets/images/pwaLogo.webp"),
			footerLink: [
				{name: "Certification", url: ""},
				{name: "Final Project", url: "https://pakistan-olx-1.firebaseapp.com/"}
			]
		}
	]
};

// Blogs Section

const blogSection = {
	
	title: "Blogs",
	subtitle: "With Love for Developing cool stuff, I love to write and teach others what I have learnt.",
	
	blogs: [
		{
			url: "https://blog.usejournal.com/create-a-google-assistant-action-and-win-a-google-t-shirt-and-cloud-credits-4a8d86d76eae",
			title: "Win a Google Assistant Tshirt and $200 in Google Cloud Credits",
			description: "Do you want to win $200 and Google Assistant Tshirt by creating a Google Assistant Action in less then 30 min?"
		},
		{
			url: "https://medium.com/@saadpasta/why-react-is-the-best-5a97563f423e",
			title: "Why REACT is The Best?",
			description: "React is a JavaScript library for building User Interface. It is maintained by Facebook and a community of individual developers and companies."
		}
	]
};

// Talks Sections

const talkSection = {
	title: "TALKS",
	subtitle: emoji("I LOVE TO SHARE MY LIMITED KNOWLEDGE AND GET A SPEAKER BADGE 😅"),
	
	talks: [
		{
			title: "Build Actions For Google Assistant",
			subtitle: "Codelab at GDG DevFest Karachi 2019",
			slides_url: "https://bit.ly/saadpasta-slides",
			event_url: "https://www.facebook.com/events/2339906106275053/"
		}
	]
};

// Podcast Section

const podcastSection = {
	title: emoji("Podcast 🎙️"),
	subtitle: "I LOVE TO TALK ABOUT MYSELF AND TECHNOLOGY",
	
	// Please Provide with Your Podcast embeded Link
	podcast: ["https://anchor.fm/codevcast/embed/episodes/DevStory---Saad-Pasta-from-Karachi--Pakistan-e9givv/a-a15itvo"]
};

const contactInfo = {
	title: emoji("Contact Me ☎️"),
	subtitle: "Discuss a project or just want to say hi? My Inbox is open for all.",
	number: "+503-62097179",
	email_address: ["rl0185032015@unab.edu.sv", "ingrivaske@gmail.com"]
};

//Twitter Section

const twitterDetails = {
	
	userName: "twitter"//Replace "twitter" with your twitter username without @
	
};
export {
	greeting,
	socialMediaLinks,
	skillsSection,
	educationInfo,
	techStack,
	workExperiences,
	openSource,
	bigProjects,
	achievementSection,
	blogSection,
	talkSection,
	podcastSection,
	contactInfo,
	twitterDetails,
	onWayProjects
};
