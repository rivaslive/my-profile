import React from "react";

const StyleContext = React.createContext();

export const StyleProvider = StyleContext.Provider;
export const StyleConsumer = StyleContext;

export default StyleContext;
