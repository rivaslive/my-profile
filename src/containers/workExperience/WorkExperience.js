import "./WorkExperience.css";
import React, {useContext} from "react";
import {Fade} from "react-awesome-reveal";
import {workExperiences} from "../../portfolio";
import StyleContext from "../../contexts/StyleContext";
import ExperienceCard from "../../components/experienceCard/ExperienceCard";

export default function WorkExperience() {
	const {isDark} = useContext(StyleContext)
	if (workExperiences.viewExperiences) {
		return (
			<div id="experience">
				<Fade bottom duration={1000} distance="20px">
					<div className="experience-container" id="workExperience">
						<div>
							<h1 className="experience-heading">Experiences</h1>
							<div className="experience-cards-div">
								{
									workExperiences.experience.map((card, i) =>
										<ExperienceCard key={i} isDark={isDark} cardInfo={card}/>
									)}
							</div>
						</div>
					</div>
				</Fade>
			</div>
		);
	}
	return null;
}
