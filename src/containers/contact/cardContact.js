import React from 'react';
import './Contact.css'

const CardContact = ({number, title, isDark, email = false, index}) => {
  console.log(number);
  return <div className={`card-contact ${isDark ? 'dark' : ''}`}>
		<a href={`${email ? 'mailto:' : 'tel:'} ${number}`}>
			<div className='card-contact-body'>
				<div className='card-contact-title'>{title} {email && index + 1}:</div>
				<div className='card-contact-subtitle'>{number}</div>
			</div>
		</a>
	</div>
}

export default CardContact;
