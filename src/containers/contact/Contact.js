import "./Contact.css";
import React, {useContext} from "react";
import {Fade} from "react-awesome-reveal";
import {contactInfo} from "../../portfolio";
import StyleContext from "../../contexts/StyleContext";
import SocialMedia from "../../components/socialMedia/SocialMedia";
import CardContact from "./cardContact";

export default function Contact() {
	const {isDark} = useContext(StyleContext);
	return (
		<Fade bottom duration={1000} distance="20px">
			<div className="main contact-margin-top" id="contact">
				<div className="contact-div-main">
					<div className="contact-header">
						<h1 className="heading contact-title" style={{textAlign: 'center'}}>{contactInfo.title}</h1>
						<h3 style={{textAlign: 'center'}}>{contactInfo.subtitle}</h3>
						
						<div
							className={
								isDark ? "dark-mode contact-text-div" : "contact-text-div"
							}
						>
							<CardContact number={contactInfo.number} title='Telephone' isDark={isDark}/>
							
							{
								contactInfo.email_address.map((email, i) =>
									<CardContact key={i} number={email} title='Email' isDark={isDark} email={true} index={i}/>)
							}
						</div>
						<br/>
						<SocialMedia/>
					</div>
					<div className="contact-image-div">
						<img
							alt="Kevin Working"
							src={require(`../../assets/images/me.svg`)}
						/>
					</div>
				</div>
			</div>
		</Fade>
	);
}
