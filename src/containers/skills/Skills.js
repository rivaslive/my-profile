import "./Skills.css";
import Lottie from "lottie-react";
import React, {useContext} from "react";
import {Fade} from "react-awesome-reveal";
import {skillsSection} from "../../portfolio";
import programming from '../../assets/lottie/programming2.json'
import StyleContext from "../../contexts/StyleContext";
import SoftwareSkill from "../../components/softwareSkills/SoftwareSkill";

export default function Skills() {
	const {isDark} = useContext(StyleContext);
	return (
		<div className={isDark ? "dark-mode main" : "main"} id="skills">
			<div className="skills-main-div">
				<Fade left duration={1000}>
					<div
						className="skills-image-div"
						style={{marginRight: 'auto', marginLeft: 'auto', width: 500, maxWidth: '100%'}}>
						<Lottie animationData={programming}/>
					</div>
				</Fade>
				<Fade right duration={1000}>
					<div className="skills-text-div">
						<h1
							className={isDark ? "dark-mode skills-heading" : "skills-heading"}
						>
							{skillsSection.title}{" "}
						</h1>
						<h3
							className={
								isDark
									? "dark-mode subTitle skills-text-subtitle"
									: "subTitle skills-text-subtitle"
							}
						>
							{skillsSection.subTitle}
						</h3>
						<div>
							{skillsSection.skills.map((skills, i) => {
								return (
									<p
										key={i}
										className={
											isDark
												? "dark-mode subTitle skills-text"
												: "subTitle skills-text"
										}
									>
										{skills}
									</p>
								);
							})}
						</div>
						<SoftwareSkill/>
					</div>
				</Fade>
			</div>
		</div>
	);
}
