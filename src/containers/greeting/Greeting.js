﻿import "./Greeting.css";
import React, { useContext } from "react";
import { greeting } from "../../portfolio";
import { Fade } from "react-awesome-reveal";
import StyleContext from "../../contexts/StyleContext";
import Button from "../../components/button/Button";

export default function Greeting() {
  const { isDark } = useContext(StyleContext);
  
  return (
    <Fade bottom duration={1000} distance="40px">
      <div className="greet-main" id="greeting">
        <div className="greeting-main">
          <div className="greeting-text-div">
            <div style={{textAlign: 'center'}}>
              <h1
                className={isDark ? "dark-mode greeting-text" : "greeting-text"}
              >
                {greeting.title}
              </h1>
              <h2 style={{marginBlockStart: 0, marginBlockEnd: 0}} className={isDark ? "dark-mode greeting-text-2" : "greeting-text-2"}>D e v e l o p e r s</h2>
              <h3
                className={
                  isDark
                    ? "dark-mode greeting-text-p"
                    : "greeting-text-p subTitle"
                }
              >
                {greeting.subTitle}
              </h3>
              
              <div className='greeting-button'>
                <a href={"#contact"} style={{textDecoration: 'none'}}>
                  <Button text='Contact me'/>
                </a>
              </div>
            </div>
          </div>
          <div className="greeting-image-div">
            <img
              alt="saad sitting on table"
              src={`${process.env.PUBLIC_URL}/me.svg`}
            />
          </div>
        </div>
      </div>
    </Fade>
  );
}
