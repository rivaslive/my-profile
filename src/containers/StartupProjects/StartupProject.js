import "./StartupProjects.css";
import React, {useContext} from "react";
import {Fade} from "react-awesome-reveal";
import {bigProjects, onWayProjects} from "../../portfolio";
import StyleContext from "../../contexts/StyleContext";
import BlogCard from "../../components/blogCard/BlogCard";

export default function StartupProject() {
	const {isDark} = useContext(StyleContext);
	return (
		<Fade bottom duration={1000} distance="20px">
			<div className="main" id="project">
				<div className="blog-header">
					<h1 className="blog-header-text">{bigProjects.title}</h1>
					<h3 className={isDark ? "dark-mode" : ""}>{bigProjects.subtitle}</h3>
				</div>
				<div className="blog-main-div">
					<div className="blog-text-div">
						{bigProjects.projects.map((project, i) => {
							if (project.title === 'Hola Mundo | TimerCycle') return <BlogCard
								key={i}
								style={{backgroundColor: '#827474'}}
								isDark={isDark}
								blog={project}
							/>
							
							return (
								<BlogCard
									key={i}
									isDark={isDark}
									blog={project}
								/>
							);
						})}
					</div>
				</div>
			</div>
			
			<div className="main" id="project">
				<div className="blog-header">
					<h1 className="blog-header-text">{onWayProjects.title}</h1>
					<h3 className={isDark ? "dark-mode" : ""}>{onWayProjects.subtitle}</h3>
				</div>
				<div className="blog-main-div">
					<div className="blog-text-div">
						{onWayProjects.projects.map((project, i) => {
							return (
								<BlogCard
									key={i}
									isDark={isDark}
									blog={project}
								/>
							);
						})}
					</div>
				</div>
			</div>
		</Fade>
	);
}
